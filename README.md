# branch-learning

Dummy repo for playing with branching and integration.

Alasdair is adding another line so he can see how branches work.

Alasdair is making a local commit in master that he's going to try and migrate to a branch he hasn't yet created. Let's see how this goes...